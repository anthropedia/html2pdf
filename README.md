# Convert an HTML URL into a PDF

aka PDF print a web page

### Requirements

Bun and Weasyprint should be installed on your system

Run the server with the optional 3003 port (3000 by default):

```
bun index.ts 3003
```

Now generate a PDF from a web page and call it "example.pdf": http://localhost:3003/?url=https://www.iana.org/help/example-domains&name=example.pdf
