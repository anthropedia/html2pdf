import { unlink } from "node:fs/promises"

const port = Bun.argv[2] || Bun.env.PORT || 3000

const server = Bun.serve({
  port,
  async fetch(request) {
    const location = new URL(request.url)
    const params = (new URLSearchParams(location.search))
    const pdfFile = "/tmp/weasyprint-output"
    const filename = params.get("name") || "download.pdf"
    const command = `weasyprint ${params.get("url")} ${pdfFile}`
    await Bun.spawnSync(command.split(" "))
    const response = Bun.file(pdfFile).stream()
    unlink(pdfFile)
    return new Response(response, { headers: {
    	"Content-Type": "application/pdf",
    	"Content-Disposition": `attachment; filename=${filename}`
    }})
  },
})

console.info(`Listening on ${server.url}`)

